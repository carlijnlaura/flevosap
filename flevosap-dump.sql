# ************************************************************
# Sequel Ace SQL dump
# Version 2082
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.5.5-MariaDB)
# Database: Flevosap
# Generation Time: 2020-10-22 08:53:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;


# Dump of table admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin`
(
    `id`      int(11) NOT NULL,
    `user_id` int(11) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_admin_users1_idx` (`user_id`),
    CONSTRAINT `fk_admin_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin`
    DISABLE KEYS */;

INSERT INTO `admin` (`id`, `user_id`)
VALUES (1, 1),
       (2, 2),
       (3, 3),
       (4, 4);

/*!40000 ALTER TABLE `admin`
    ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category`
(
    `id`   int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(45) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category`
    DISABLE KEYS */;

INSERT INTO `category` (`id`, `name`)
VALUES (1, 'Vruchten'),
       (2, 'Groenten');

/*!40000 ALTER TABLE `category`
    ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table OrderedProduct
# ------------------------------------------------------------

DROP TABLE IF EXISTS `OrderedProduct`;

CREATE TABLE `OrderedProduct`
(
    `product_id` int(11) NOT NULL,
    `order_id`   int(11) NOT NULL,
    `quanity`    int(11) DEFAULT NULL,
    PRIMARY KEY (`product_id`, `order_id`),
    KEY `fk_product_has_orders_orders1_idx` (`order_id`),
    KEY `fk_product_has_orders_product_idx` (`product_id`),
    CONSTRAINT `fk_product_has_orders_orders1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
    CONSTRAINT `fk_product_has_orders_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders`
(
    `id`           INT          NOT NULL AUTO_INCREMENT,
    `orderdate`    TIMESTAMP    NULL DEFAULT NULL,
    `shippingDate` TIMESTAMP    NULL DEFAULT NULL,
    `status`       INT          NULL DEFAULT NULL,
    `user_id`      INT          NOT NULL,
    `first_name`   VARCHAR(255) NULL,
    `last_name`    VARCHAR(255) NULL,
    `phone_number` INT          NULL,
    `zip`          VARCHAR(15)  NULL,
    `street`       VARCHAR(255) NULL,
    `city`         VARCHAR(255) NULL,
    `email`        VARCHAR(255) NULL,
    PRIMARY KEY (`id`),
    KEY `fk_orders_users1_idx` (`user_id`),
    CONSTRAINT `fk_orders_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



# Dump of table product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product`
(
    `id`          int(11)     NOT NULL AUTO_INCREMENT,
    `name`        varchar(45) NOT NULL,
    `price`       double      NOT NULL,
    `image`       varchar(255) DEFAULT NULL,
    `ingredients` varchar(45)  DEFAULT NULL,
    `category_id` int(11)     NOT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_product_category1_idx` (`category_id`),
    CONSTRAINT `fk_product_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product`
    DISABLE KEYS */;

INSERT INTO `product` (`id`, `name`, `price`, `image`, `ingredients`, `category_id`)
VALUES (1, 'Appel Cranberry', 2, '/assets/upload/Appel_Cranberry.png', 'Appel, Cranberry', 1),
       (2, 'Appel Ananas', 5, '/assets/upload/Flevosap-Appel-Ananas.jpg', 'Appel, Ananas', 1),
       (3, 'Appel aardbei', 1, '/assets/upload/Flevosap-appel-aardbei.jpg', 'Appel, aardbei', 1),
       (4, 'Appel cassis', 3, '/assets/upload/Flevosap-appel-cassis.jpg', 'Appel, cassis', 1),
       (5, 'Appel citroen', 6, '/assets/upload/Flevosap-appel-citroen.jpg', 'Appel, citroen', 1),
       (6, 'Appel kers', 2, '/assets/upload/Flevosap-appel-kers.jpg', 'Appel, kers', 1),
       (7, 'Appel peer zwartebes', 3, '/assets/upload/Flevosap-appel-peer-zwartebes.jpg', 'Appel, peer, zwartebes', 1),
       (8, 'Appel peer', 1, '/assets/upload/Flevosap-appel-peer.jpg', 'Appel, peer', 1),
       (9, 'Appel sinaasappel', 41, '/assets/upload/Flevosap-appel-sinaasappel.jpg', 'Appel, sinaasappel', 1),
       (10, 'Appel', 1, '/assets/upload/Flevosap-appel.jpg', 'Appel', 1),
       (11, 'Peer cranberry', 3, '/assets/upload/Flevosap-peer-cranberry.jpg', 'Peer, cranberry', 1),
       (12, 'Sinaasappel', 4, '/assets/upload/Flevosap-sinaasappel.jpg', 'Sinaasappel', 1),
       (13, 'Appel rabarber', 21, '/assets/upload/Winteravond-aangepast.jpg', 'Appel, rabarber', 1),
       (14, 'Peer', 2, '/assets/upload/appel-rabarber.jpg', 'Peer', 1),
       (15, 'Kleintje appel', 1, '/assets/upload/kleintje-appel.jpg', 'Appel', 1);

/*!40000 ALTER TABLE `product`
    ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table review
# ------------------------------------------------------------

DROP TABLE IF EXISTS `review`;

CREATE TABLE `review`
(
    `id`         int(11) NOT NULL AUTO_INCREMENT,
    `columns`    varchar(45) DEFAULT NULL,
    `text`       varchar(45) DEFAULT NULL,
    `stars`      varchar(45) DEFAULT NULL,
    `created_at` varchar(45) DEFAULT NULL,
    `product_id` int(11) NOT NULL,
    `user_id`    int(11) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_review_product1_idx` (`product_id`),
    KEY `fk_review_users1_idx` (`user_id`),
    CONSTRAINT `fk_review_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
    CONSTRAINT `fk_review_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review`
    DISABLE KEYS */;

INSERT INTO `review` (`id`, `columns`, `text`, `stars`, `created_at`, `product_id`, `user_id`)
VALUES (1, NULL, 'sed nisl nunc rhoncus dui', '4', '2019-10-30 17:15:26', 8, 5),
       (2, NULL, 'eu mi nulla ac', '4', '2020-07-14 14:23:42', 3, 5),
       (3, NULL, 'sem duis aliquam convallis nunc', '3', '2020-03-21 03:11:22', 12, 5),
       (4, NULL, 'congue vivamus metus arcu adipiscing', '2', '2020-04-13 00:22:32', 2, 2),
       (5, NULL, 'sit amet sapien dignissim', '3', '2019-10-28 18:13:25', 11, 2),
       (6, NULL, 'platea dictumst maecenas ut', '5', '2020-04-29 10:43:33', 3, 4),
       (7, NULL, 'massa donec', '2', '2019-10-30 08:40:26', 8, 2),
       (8, NULL, 'magnis dis parturient montes', '2', '2020-10-14 19:32:44', 3, 1),
       (9, NULL, 'pretium nisl ut volutpat', '2', '2020-03-10 22:31:38', 5, 3),
       (10, NULL, 'et ultrices posuere cubilia', '3', '2020-09-05 23:35:27', 12, 2),
       (11, NULL, 'rutrum ac lobortis vel dapibus', '3', '2019-11-23 21:30:00', 4, 2),
       (12, NULL, 'odio in hac habitasse platea', '4', '2020-04-08 03:39:37', 11, 2),
       (13, NULL, 'integer tincidunt ante', '3', '2020-07-30 23:17:25', 7, 3),
       (14, NULL, 'metus aenean fermentum donec ut', '1', '2020-06-08 23:43:04', 10, 1),
       (15, NULL, 'integer ac neque duis', '3', '2020-02-28 07:23:31', 5, 5),
       (16, NULL, 'duis ac nibh', '3', '2019-12-19 05:38:17', 9, 1),
       (17, NULL, 'egestas metus', '5', '2020-08-16 00:12:29', 7, 5),
       (18, NULL, 'vehicula condimentum curabitur in libero', '1', '2020-06-19 22:49:27', 10, 1),
       (19, NULL, 'risus auctor sed tristique', '1', '2020-06-18 17:27:58', 8, 1),
       (20, NULL, 'lacus at', '3', '2020-10-17 07:22:58', 15, 4),
       (21, NULL, 'lorem integer', '5', '2020-07-20 23:24:49', 4, 3),
       (22, NULL, 'dignissim vestibulum', '1', '2020-07-10 05:42:31', 6, 5),
       (23, NULL, 'turpis a pede posuere nonummy', '4', '2020-07-23 16:43:35', 1, 3),
       (24, NULL, 'lacinia aenean', '2', '2020-10-09 09:04:48', 4, 5),
       (25, NULL, 'lectus in quam', '4', '2020-08-03 16:32:57', 7, 5),
       (26, NULL, 'in sagittis dui vel nisl', '4', '2019-12-24 21:29:41', 9, 2),
       (27, NULL, 'maecenas leo odio condimentum id', '3', '2020-01-15 04:14:16', 2, 4),
       (28, NULL, 'nulla nisl', '1', '2020-07-10 03:55:21', 4, 2),
       (29, NULL, 'vitae nisl aenean lectus', '2', '2020-03-11 22:50:27', 15, 4),
       (30, NULL, 'vestibulum ante ipsum', '3', '2020-06-14 11:16:00', 9, 1),
       (31, NULL, 'potenti nullam porttitor lacus at', '2', '2019-12-26 22:03:20', 15, 2),
       (32, NULL, 'ullamcorper purus', '2', '2019-12-25 02:50:18', 4, 3),
       (33, NULL, 'nam dui proin leo', '2', '2020-05-24 15:49:52', 3, 1),
       (34, NULL, 'nisi volutpat eleifend donec ut', '3', '2019-12-31 23:07:22', 11, 2),
       (35, NULL, 'posuere cubilia curae duis', '3', '2020-07-05 23:02:00', 7, 1),
       (36, NULL, 'ipsum aliquam non mauris', '2', '2020-05-29 15:57:24', 4, 2),
       (37, NULL, 'ligula sit', '1', '2020-05-20 10:48:28', 4, 3),
       (38, NULL, 'enim lorem ipsum', '3', '2020-07-15 13:26:53', 9, 2),
       (39, NULL, 'donec pharetra magna vestibulum aliquet', '3', '2020-04-28 20:03:42', 12, 5),
       (40, NULL, 'odio justo', '1', '2020-09-27 12:55:57', 2, 5),
       (41, NULL, 'pretium quis lectus suspendisse', '5', '2019-12-04 15:58:29', 5, 2),
       (42, NULL, 'felis ut at dolor quis', '2', '2020-08-28 10:24:37', 10, 3),
       (43, NULL, 'venenatis lacinia', '5', '2020-06-05 19:44:59', 12, 5),
       (44, NULL, 'rutrum nulla tellus in sagittis', '3', '2019-10-27 13:51:54', 9, 2),
       (45, NULL, 'platea dictumst morbi', '1', '2020-09-02 22:59:10', 8, 5),
       (46, NULL, 'primis in faucibus', '5', '2020-04-21 15:17:51', 3, 3),
       (47, NULL, 'purus eu magna', '5', '2019-11-05 09:31:25', 13, 5),
       (48, NULL, 'sapien cum', '1', '2019-11-04 19:11:57', 11, 4),
       (49, NULL, 'dui nec nisi', '5', '2019-11-21 17:23:46', 6, 2),
       (50, NULL, 'ac est lacinia nisi', '3', '2020-07-19 22:12:07', 7, 1),
       (51, NULL, 'duis mattis egestas metus', '5', '2020-04-25 23:44:25', 9, 1),
       (52, NULL, 'est donec odio justo', '4', '2020-02-01 09:41:16', 3, 1),
       (53, NULL, 'non mi', '4', '2020-07-20 00:06:13', 7, 2),
       (54, NULL, 'justo nec condimentum neque sapien', '5', '2020-08-06 16:41:17', 13, 1),
       (55, NULL, 'tortor id nulla ultrices', '2', '2020-08-18 05:59:31', 13, 5),
       (56, NULL, 'vitae nisl aenean lectus', '2', '2020-08-24 08:39:15', 11, 3),
       (57, NULL, 'phasellus in felis', '5', '2020-02-23 07:21:31', 10, 1),
       (58, NULL, 'aenean sit', '2', '2020-06-05 11:04:09', 15, 3),
       (59, NULL, 'vestibulum ac', '2', '2020-03-26 03:59:31', 14, 2),
       (60, NULL, 'ut massa quis', '2', '2019-10-30 22:59:54', 10, 2),
       (61, NULL, 'sit amet consectetuer', '5', '2019-10-23 03:48:12', 9, 1),
       (62, NULL, 'eget rutrum', '5', '2020-02-15 09:09:04', 4, 2),
       (63, NULL, 'in purus', '3', '2020-02-19 07:39:22', 6, 2),
       (64, NULL, 'ut ultrices vel augue', '3', '2020-09-16 16:43:13', 15, 1),
       (65, NULL, 'lacus curabitur', '2', '2020-01-12 01:21:37', 8, 1),
       (66, NULL, 'mattis nibh ligula nec', '5', '2020-02-28 12:52:46', 3, 2),
       (67, NULL, 'id sapien in sapien', '1', '2019-12-02 02:32:50', 9, 2),
       (68, NULL, 'luctus tincidunt nulla', '1', '2020-05-30 07:19:38', 5, 5),
       (69, NULL, 'ligula sit amet', '3', '2020-06-26 15:18:56', 2, 2),
       (70, NULL, 'porttitor lorem', '2', '2020-08-12 06:18:33', 14, 2),
       (71, NULL, 'tincidunt eu felis', '1', '2020-04-06 12:27:02', 8, 2),
       (72, NULL, 'nulla sed', '3', '2020-03-24 06:14:34', 9, 4),
       (73, NULL, 'adipiscing elit proin risus praesent', '2', '2020-02-06 19:29:34', 8, 4),
       (74, NULL, 'sed lacus morbi', '5', '2019-12-26 08:12:31', 4, 5),
       (75, NULL, 'varius integer ac leo', '1', '2020-04-04 22:15:41', 9, 1),
       (76, NULL, 'morbi vel lectus', '4', '2020-03-17 03:11:34', 4, 4),
       (77, NULL, 'morbi non lectus', '4', '2020-05-29 17:21:56', 1, 5),
       (78, NULL, 'in quis justo maecenas rhoncus', '4', '2020-08-08 20:19:17', 9, 1),
       (79, NULL, 'sollicitudin ut suscipit', '2', '2020-04-21 09:05:39', 2, 1),
       (80, NULL, 'volutpat eleifend donec ut', '3', '2020-09-30 05:41:23', 1, 2),
       (81, NULL, 'sit amet', '1', '2019-11-26 04:17:59', 13, 4),
       (82, NULL, 'eget tincidunt eget tempus', '5', '2020-07-30 07:04:36', 8, 4),
       (83, NULL, 'tortor duis', '4', '2020-02-24 17:07:31', 6, 2),
       (84, NULL, 'ante ipsum primis in faucibus', '3', '2020-07-30 04:54:13', 1, 2),
       (85, NULL, 'et eros vestibulum', '4', '2019-11-11 20:33:25', 9, 4),
       (86, NULL, 'morbi ut', '2', '2020-06-27 19:05:30', 12, 1),
       (87, NULL, 'eleifend pede libero quis orci', '4', '2019-11-14 05:13:54', 5, 2),
       (88, NULL, 'ornare imperdiet sapien urna', '3', '2019-10-28 08:37:56', 9, 5),
       (89, NULL, 'pellentesque viverra pede', '2', '2020-06-28 15:44:06', 13, 1),
       (90, NULL, 'nulla integer', '3', '2019-12-08 13:51:01', 15, 3),
       (91, NULL, 'et magnis dis parturient', '3', '2020-08-23 04:16:40', 13, 5),
       (92, NULL, 'arcu libero rutrum ac', '3', '2020-02-03 07:31:44', 14, 1),
       (93, NULL, 'erat curabitur gravida', '2', '2020-09-01 13:49:44', 6, 1),
       (94, NULL, 'nibh fusce lacus purus aliquet', '3', '2020-05-24 01:42:00', 3, 4),
       (95, NULL, 'ornare consequat lectus in', '5', '2019-12-02 00:37:55', 13, 4),
       (96, NULL, 'sit amet nulla quisque', '1', '2020-03-13 23:01:50', 11, 4),
       (97, NULL, 'lectus pellentesque', '1', '2020-07-27 18:45:23', 2, 1),
       (98, NULL, 'quis odio consequat varius integer', '5', '2020-09-10 06:41:23', 3, 1),
       (99, NULL, 'pellentesque quisque porta volutpat', '5', '2019-10-29 16:18:48', 2, 5),
       (100, NULL, 'praesent blandit', '2', '2020-05-15 03:14:44', 8, 1);

/*!40000 ALTER TABLE `review`
    ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shoppingcart
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shoppingcart`;

CREATE TABLE `shoppingcart`
(
    `product_id`     int(11) NOT NULL AUTO_INCREMENT,
    `carttotal`      float      DEFAULT NULL,
    `discountcode`   tinyint(4) DEFAULT NULL,
    `discountamount` int(11)    DEFAULT NULL,
    `id`             int(11) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_shoppingcart_product1_idx` (`product_id`),
    CONSTRAINT `fk_shoppingcart_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users`
(
    `id`           int(11)      NOT NULL AUTO_INCREMENT,
    `email`        varchar(255) NOT NULL,
    `password`     varchar(255) NOT NULL,
    `first_name`   varchar(255) NOT NULL,
    `last_name`    varchar(255) NOT NULL,
    `phone_number` varchar(100) NOT NULL,
    `zip`          varchar(8)   NOT NULL,
    `street`       varchar(255) NOT NULL,
    `city`         varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users`
    DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `password`, `first_name`, `last_name`, `phone_number`, `zip`, `street`, `city`)
VALUES (1, 'fake@flevosap.nl', '$2y$10$OFLFL0fmt4ra1H5kDspu.u/uArrq1v9TEOb54nw4JEKB/6BabgK6W', 'Fake', 'Flevosap',
        '0884696600', '1315 RC', 'Hospitaaldreef 5', 'Almere'),
       (2, 'rudy@flevosap.nl', '$2y$10$W9rU45kDtOIlpMZWHKcFm.qrRC/ANPE.1KKMVYKb/4ZxVKleejuXe', 'Rudy', 'Flevosap',
        '0884696602', '1315 RC', 'Hospitaaldreef 5', 'Almere'),
       (3, 'stephan@flevosap.nl', '$2y$10$iz.ofo1TtMGr8vHnPz.NfuOYD2s7ByEvjFJzIjB28PUFgbvwAmzRa', 'Stephan', 'Flevosap',
        '0884696601', '1315 RC', 'Hospitaaldreef 5', 'Almere'),
       (4, 'matthijs@flevosap.nl', '$2y$10$OOPEfNLObpNKXdwCLYZ9X.tiQo0isRmKZeO1w0D4pL/NUnpMC3AdW', 'Matthijs',
        'Flevosap', '0884696603', '1315 RC', 'Hospitaaldreef 5', 'Almere'),
       (5, 'john@email.nl', '$2y$10$KRxioome4xuTruXu3wQ6yuIr5JH1z6cZrksvosGTEHBm1ZIfSulV2', 'Matthijs', 'Flevosap',
        '0884696604', '1315 RC', 'Hospitaaldreef 6', 'Almere');

/*!40000 ALTER TABLE `users`
    ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table warehouse
# ------------------------------------------------------------

DROP TABLE IF EXISTS `warehouse`;

CREATE TABLE `warehouse`
(
    `order_id` int(11) NOT NULL AUTO_INCREMENT,
    KEY `fk_warehouse_orders1_idx` (`order_id`),
    CONSTRAINT `fk_warehouse_orders1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
