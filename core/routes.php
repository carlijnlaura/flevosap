<?php

$routes = [
    'GET' => [
        '' => [
            'controller' => 'HomeController',
            'method' => 'index'
        ],
        'users' => [
            'controller' => 'UserController',
            'method' => 'index',
            'auth' => true,
            'admin' => true,
        ],
        'order/complete' => [
            'controller' => 'ShoppingcartController',
            'method' => 'orderComplete',
        ],
        'cart' => [
            'controller' => 'ShoppingcartController',
            'method' => 'view',
        ],
        'login' => [
            'controller' => 'LoginController',
            'method' => 'getLoginView'
        ],
        'register' => [
            'controller' => 'RegisterController',
            'method' => 'getRegisterView'
        ],
        'logout' => [
            'controller' => 'LoginController',
            'method' => 'logout'
        ],
        'profile' => [
            'controller' => 'ProfileController',
            'method' => 'view',
            'auth' => true,
        ],
        'admin' => [
            'controller' => 'AdminController',
            'method' => 'index',
            'auth' => true,
            'admin' => true,
        ],
        'product' => [
            'controller' => 'ProductController',
            'method' => 'view'
        ],
        'product/create' => [
            'controller' => 'ProductController',
            'method' => 'getCreate',
            'auth' => true,
            'admin' => true,
        ],
        'admin/admins/add-list' => [
            'controller' => 'AdminController',
            'method' => 'showUsers',
            'auth' => true,
            'admin' => true,
        ],
        'admin/orders/view' => [
            'controller' => 'OrderController',
            'method' => 'show',
            'auth' => true,
            'admin' => true,
        ],
        'admin/admins' => [
            'controller' => 'AdminController',
            'method' => 'adminindex',
            'auth' => true,
            'admin' => true,
        ],
        'admin/admins/edit' => [
            'controller' => 'AdminController',
            'method' => 'adminedit',
            'auth' => true,
            'admin' => true,
        ],
        'search' => [
            'controller' => 'ProductController',
            'method' => 'search'
        ],
        'user/orders' => [
            'controller' => 'OrderController',
            'method' => 'index2',
            'auth' => true,
        ],
        'user/orders/view' => [
            'controller' => 'OrderController',
            'method' => 'show2',
            'auth' => true,
        ],
        'admin/orders' => [
            'controller' => 'OrderController',
            'method' => 'index',
            'auth' => true,
            'admin' => true,
        ],
        'admin/users/view' => [
            'controller' => 'AdminController',
            'method' => 'view',
            'auth' => true,
            'admin' => true,
        ],
        'order/create' => [
            'controller' => 'ShoppingcartController',
            'method' => 'createOrder',
        ],
        'cart/remove' => [
            'controller' => 'ShoppingcartController',
            'method' => 'remove',
        ],
    ],
    'POST' => [
        'save-users' => [
            'controller' => 'UserController',
            'method' => 'save',
            'auth' => true,
            'admin' => true,
        ],
        'reviews/create' => [
            'controller' => 'HomeController',
            'method' => 'index',
            'auth' => true,
        ],
        'login-post' => [
            'controller' => 'LoginController',
            'method' => 'login'
        ],
        'register-post' => [
            'controller' => 'RegisterController',
            'method' => 'register'
        ],
        'profile/update' => [
            'controller' => 'ProfileController',
            'method' => 'update',
            'auth' => true,
        ],
        'admin/admins/add' => [
            'controller' => 'AdminController',
            'method' => 'addUserAsAdmin',
            'auth' => true,
            'admin' => true,
        ],
        'admin/admins/update' => [
            'controller' => 'AdminController',
            'method' => 'adminupdate',
            'auth' => true,
            'admin' => true,
        ],
        'create-product' => [
            'controller' => 'ProductController',
            'method' => 'create',
            'auth' => true,
            'admin' => true,
        ],
        'edit-product' => [
            'controller' => 'ProductController',
            'method' => 'edit',
            'auth' => true,
            'admin' => true,
        ],
        'save-product' => [
            'controller' => 'ProductController',
            'method' => 'save',
            'auth' => true,
            'admin' => true,
        ],

        'admin/user/update' => [
            'controller' => 'AdminController',
            'method' => 'update',
            'auth' => true,
            'admin' => true,
        ],
        'review/create' => [
            'controller' => 'ReviewController',
            'method' => 'createReview',
            'auth' => true,
        ],
        'cart/add' => [
            'controller' => 'ShoppingcartController',
            'method' => 'add',
        ],
        'edit-users' => [
            'controller' => 'UserController',
            'method' => 'edit',
            'auth' => true,
        ],
        'remove-users' => [
            'controller' => 'UserController',
            'method' => 'remove',
            'auth' => true,
            'admin' => true,
        ],
        'profile/delete' => [
            'controller' => 'ProfileController',
            'method' => 'delete',
            'auth' => true,
        ],
        'admin/user/delete' => [
            'controller' => 'AdminController',
            'method' => 'delete',
            'auth' => true,
            'admin' => true,
        ],
        'remove-product' => [
            'controller' => 'ProductController',
            'method' => 'removeProduct',
            'auth' => true,
            'admin' => true,
        ],
        'order/createpost' => [
            'controller' => 'ShoppingcartController',
            'method' => 'createOrderPost',
        ],
    ],
];