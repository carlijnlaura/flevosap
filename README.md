#Flevosap handleiding 

#Database
Er zijn 2 bestanden die voor de database zijn bedoeld. Je moet 1 van de 2 gebruiken
om een goede lokale omgeving op te zetten.

#####flevosap.sql = alleen database structuur
#####flevosap-dump.sql = database structuur inclusief data

#Video files
Om de bestaande producten in de database goed werkend te krijgen moet je via
https://drive.google.com/drive/folders/1zNOzWPc7lmpHzlQR6f3S5lrYp4-hx0VJ?usp=sharing
de images downloaden.
De images plaats je in de /assets/upload folder in de root van het project.
Indien de folder niet bestaat moet je die aanmaken.
De resultaat wordt dan:

/assets/upload/Appel_Cranberry.png

/assets/upload/Flevosap-appel-cassis.jpg

.etc..

#Start server
> php -S localhost:9999

#Auth gegevens
| email              | password   | admin |
|--------------------|------------|-------|
| fake@flevosap.nl     | 48T03GyvcD | yes   |
| stephan@flevosap.nl  | y3ssZxqkvd | yes   |
| rudy@flevosap.nl     | VlzL09NimN | yes   |
| matthijs@flevosap.nl | 5UykUiePPU | yes   |
| john@email.com     | D37bYa8lGH | no    |

> Database login gegevens staan in de /models/BaseModel.php