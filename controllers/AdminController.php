<?php

class AdminController
{

    public function index()
    {
        $products = Product::getallproducts(); //Variabele products is klasse Product is functie get allproducts
        require 'views/admin.view.php';

    }

    public function view()
    {
        $users = User::AllUsersAdmin();
        require 'views/admin/users.view.php';
    }

    public function update()
    {
        $users = User::updateUsersAdmin($_POST);
        header('Location: /admin/users/view');

    }

    public function delete()
    {
        $users = User::deleteUsersAdmin($_POST['id']);
        header('Location: /admin/users/view');
    }

    public function adminindex()
    {
        $admins = Admin::all();
        require 'views/admin/admins.view.php';
    }

    public function adminedit()
    {
        $admin = Admin::find($_GET['admin_id']);
        require 'views/admin/edit_admin.view.php';
    }

    public function adminupdate()
    {
        $admin = Admin::find($_GET['admin_id']);
        if (!isset($_POST['is_admin'])) {
            $admin->delete();
        } else {
            $admin->update($_POST);
        }
        header('Location: /admin/admins');
    }

    public function showUsers()
    {
        $nonadmins = User::AllUser();
        $users = [];
        foreach ($nonadmins as $user) {
            if (!$user->isAdmin()) {
                $users[] = $user;
            }
        }
        require 'views/admin/add.view.php';
    }

    public function addUserAsAdmin()
    {
        if (isset($_POST['users'])) {
            foreach ($_POST['users'] as $userid) {
                $user = User::getUser($userid);
                $user->makeAdmin();
            }
        }
        header('Location: /admin/admins');
    }

}