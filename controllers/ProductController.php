<?php

class ProductController
{

    public function view()
    {
        $product = Product::getProduct($_GET['id']);
        $reviews = Review::getReviewsForProduct($_GET['id']);

        $review_total = 0;
        $stars = 0;

        foreach ($reviews as $review) {
            $review_total += $review['stars'];
        }

        if (count($reviews) > 0) {
            $stars = $review_total / count($reviews);
        }
        require 'views/product.view.php';
    }

    public function removeProduct()
    {
        $product = Product::getProduct($_POST['id']);
        $product->delete();
        header('Location: /admin');
    }

    public function edit()
    {
        $product = Product::getProduct($_POST['id']);
        require 'views/productEdit.view.php';
    }

    public function create()
    {
        $product = Product::create($_POST);
        header('Location: /admin');
    }

    public function getCreate()
    {
        require 'views/productform.view.php';
    }

    public function save()
    {
        $product = Product::getProduct($_POST['id']);
        $product->updateProduct([
            'name' => $_POST['name'],
            'price' => $_POST['price'],
            'ingredients' => $_POST['ingredients'],
        ]);
        header('Location: /admin');
    }

    public function search()
    {
        $products = Product::searchProducts($_GET['q'] ?? "");
        require './views/search.view.php';
    }

}