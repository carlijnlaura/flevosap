<?php

class RegisterController
{

    public function getRegisterView()
    {
        require 'views/register.view.php';
    }

    public function register()
    {
        $user = User::createUser($_POST);
        $_SESSION['user'] = $user;
        mail($user->email, 'Nieuw account', 'Welkom bij FlevoSap broski');
        header('Location: /');
    }

}