<?php

class ProfileController
{
    public function view()
    {
//        $profileModel = new Profile();
//        $profile = $profileModel->viewUserInformation($_SESSION['user']->id);
        require 'views/profile.view.php';

    }

    public function update()
    {
        $user = User::updateUser($_POST);
        if ($user) {
            $_SESSION['user'] = $user;
            header('Location: /profile?msg=Profile updated&type=success');
        }
    }

    public function delete()
    {
        $user = User::deleteUser($_POST['id']);
        session_destroy();
        header('Location: /login?msg=Account deleted&type=success');
    }
}