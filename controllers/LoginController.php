<?php

class LoginController
{

    public function getLoginView()
    {
        require 'views/login.view.php';
    }

    public function login()
    {
        $user = User::tryLogin($_POST['email'], $_POST['password']);
        if ($user) {
            $_SESSION['user'] = $user;
            header('Location: /');
        }
    }

    public function logout()
    {
        session_destroy();
        header('Location: /');
    }

}