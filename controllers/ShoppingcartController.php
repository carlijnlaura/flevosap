<?php

class ShoppingcartController
{

    public function add()
    {
        $product = Product::getProduct($_POST['id']);
        if(isset($_SESSION['cart'][$product->id])) {
            $_SESSION['cart'][$product->id]['amount'] = $_SESSION['cart'][$product->id]['amount'] + $_POST['amount'];
        } else {
            $_SESSION['cart'][$product->id][] = $product;
            $_SESSION['cart'][$product->id]['amount'] = $_POST['amount'];
        }
        header('Location: /cart');
    }

    public function view()
    {
        $total = 0;
        foreach($_SESSION['cart'] as $product) {
            $total += $product[0]->price * $product['amount'];
        }
        require 'views/shopping_cart.view.php';
    }

    public function remove()
    {
        unset($_SESSION['cart'][$_GET['product_id']]);
        header('Location: /cart');
    }

    public function createOrder()
    {
        require 'views/create_order.view.php';
//        $order = Order::create([
//            'orderdate' => date("Y-m-d H:i:s"),
//            'status' => 1,
//        ]);
//
//        foreach($_SESSION['cart'] as $product) {
//            OrderedProduct::create([
//                'product_id' => $product[0]->id,
//                'order_id' => $order->id,
//                'quantity' => $product['amount'],
//            ]);
//        }
//
//        unset($_SESSION['cart']);
//
//        //Todo:: link to order page
//        header('Location: /cart');
    }

    public function createOrderPost()
    {
        if (isset($_POST['createaccount']) && !User::checkUserNoLocation($_POST['email'])) {
            $user = User::createUser([
                'email' => $_POST['email'],
                'first_name' => $_POST['first_name'],
                'last_name' => $_POST['last_name'],
                'phone_number' => $_POST['phone_number'],
                'zip' => $_POST['zip'],
                'street' => $_POST['street'],
                'city' => $_POST['city'],
                'password' => $_POST['password'],
            ]);
            $user_id = $user->id;
        } else {
            if(isset($_SESSION['user'])) {
                $user_id = $_SESSION['user']->id;
            } else {
                $user_id = 0;
            }
        }

        $order = Order::create([
            'orderdate' => date("Y-m-d H:i:s"),
            'status' => 1,
            'email' => $_POST['email'],
            'first_name' => $_POST['first_name'],
            'last_name' => $_POST['last_name'],
            'phone_number' => $_POST['phone_number'],
            'zip' => $_POST['zip'],
            'street' => $_POST['street'],
            'city' => $_POST['city'],
            'user_id' => $user_id,
        ]);

        foreach ($_SESSION['cart'] as $product) {
            OrderedProduct::create([
                'product_id' => $product[0]->id,
                'order_id' => $order->id,
                'quantity' => $product['amount'],
            ]);
        }

        unset($_SESSION['cart']);

        header('Location: /order/complete');
    }

    public function orderComplete()
    {
        require 'views/order_complete.view.php';
    }


}