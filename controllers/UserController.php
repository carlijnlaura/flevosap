<?php


class UserController
{

    public function index()
    {
        $users = User::AllUser();
        require 'views/user.view.php';

    }

    public function remove()
    {
        User::removeUser($_POST['id']);
        header('Location: /users');
    }

    public function edit()
    {
        $user = User::getUser($_POST['id']);
        require 'views/profile.view.php';
    }

    public function save()
    {
        $data = [
            'email' => $_POST['email'],
            'password' => $_POST['password'],
            'name' => $_POST['name'],
            'lastname' => $_POST['lastname'],
            'phoneNumber' => $_POST['phoneNumber'],
            'postalCode' => $_POST['postalCode'],
            'street' => $_POST['street'],

        ];
        $user = User::getUser($_POST['id']);
        $user->EditUser($data);
        header('Location: /users');
    }

}