<?php

class ReviewController
{

    public function createReview()
    {
        if($_POST['stars'] > 5 || $_POST['stars'] < 1) {
            header('Location: /product?id=' . $_GET['product_id']);
            exit;
        }
        Review::createReview($_POST['text'], $_POST['stars'], $_GET['product_id'], $_SESSION['user']->id);
        header('Location: /product?id=' . $_GET['product_id']);
    }

}