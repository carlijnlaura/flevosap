-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE =
        'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema default_schema
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema Flevosap
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `Flevosap`;

-- -----------------------------------------------------
-- Schema Flevosap
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Flevosap` DEFAULT CHARACTER SET utf8;
USE `Flevosap`;

-- -----------------------------------------------------
-- Table `Flevosap`.`category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Flevosap`.`category`;

CREATE TABLE IF NOT EXISTS `Flevosap`.`category`
(
    `id`   INT         NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Flevosap`.`product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Flevosap`.`product`;

CREATE TABLE IF NOT EXISTS `Flevosap`.`product`
(
    `id`          INT          NOT NULL AUTO_INCREMENT,
    `name`        VARCHAR(45)  NOT NULL,
    `price`       DOUBLE       NOT NULL,
    `image`       VARCHAR(255) NULL DEFAULT NULL,
    `ingredients` VARCHAR(45)  NULL DEFAULT NULL,
    `category_id` INT          NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_product_category1_idx` (`category_id` ASC),
    CONSTRAINT `fk_product_category1`
        FOREIGN KEY (`category_id`)
            REFERENCES `Flevosap`.`category` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Flevosap`.`shoppingcart`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Flevosap`.`shoppingcart`;

CREATE TABLE IF NOT EXISTS `Flevosap`.`shoppingcart`
(
    `product_id`     INT     NOT NULL AUTO_INCREMENT,
    `carttotal`      FLOAT   NULL DEFAULT NULL,
    `discountcode`   TINYINT NULL DEFAULT NULL,
    `discountamount` INT     NULL DEFAULT NULL,
    `id`             INT     NOT NULL,
    INDEX `fk_shoppingcart_product1_idx` (`product_id` ASC),
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_shoppingcart_product1`
        FOREIGN KEY (`product_id`)
            REFERENCES `Flevosap`.`product` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Flevosap`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Flevosap`.`users`;

CREATE TABLE IF NOT EXISTS `Flevosap`.`users`
(
    `id`           INT          NOT NULL AUTO_INCREMENT,
    `email`        VARCHAR(255) NOT NULL,
    `password`     VARCHAR(255) NOT NULL,
    `first_name`   VARCHAR(255) NOT NULL,
    `last_name`    VARCHAR(255) NOT NULL,
    `phone_number` VARCHAR(100) NOT NULL,
    `zip`          VARCHAR(8)   NOT NULL,
    `street`       VARCHAR(255) NOT NULL,
    `city`         VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `email_UNIQUE` (`email` ASC)
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Flevosap`.`orders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Flevosap`.`orders`;

CREATE TABLE IF NOT EXISTS `Flevosap`.`orders`
(
    `id` INT NOT NULL AUTO_INCREMENT,
    `orderdate` TIMESTAMP NULL DEFAULT NULL,
    `shippingDate` TIMESTAMP NULL DEFAULT NULL,
    `status` INT NULL DEFAULT NULL,
    `user_id` INT NOT NULL,
    `first_name` VARCHAR(255) NULL,
    `last_name` VARCHAR(255) NULL,
    `phone_number` INT NULL,
    `zip` VARCHAR(15) NULL,
    `street` VARCHAR(255) NULL,
    `city` VARCHAR(255) NULL,
    `email` VARCHAR(255) NULL,

    PRIMARY KEY (`id`),
    INDEX `fk_orders_users1_idx` (`user_id` ASC),
    CONSTRAINT `fk_orders_users1`
        FOREIGN KEY (`user_id`)
            REFERENCES `Flevosap`.`users` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Flevosap`.`admin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Flevosap`.`admin`;

CREATE TABLE IF NOT EXISTS `Flevosap`.`admin`
(
    `id`      INT NOT NULL AUTO_INCREMENT,
    `user_id` INT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_admin_users1_idx` (`user_id` ASC),
    CONSTRAINT `fk_admin_users1`
        FOREIGN KEY (`user_id`)
            REFERENCES `Flevosap`.`users` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Flevosap`.`warehouse`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Flevosap`.`warehouse`;

CREATE TABLE IF NOT EXISTS `Flevosap`.`warehouse`
(
    `order_id` INT NOT NULL AUTO_INCREMENT,
    INDEX `fk_warehouse_orders1_idx` (`order_id` ASC),
    CONSTRAINT `fk_warehouse_orders1`
        FOREIGN KEY (`order_id`)
            REFERENCES `Flevosap`.`orders` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Flevosap`.`OrderedProduct`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Flevosap`.`OrderedProduct`;

CREATE TABLE IF NOT EXISTS `Flevosap`.`OrderedProduct`
(
    `product_id` INT NOT NULL,
    `order_id`   INT NOT NULL,
    `quanity`    INT NULL DEFAULT NULL,
    PRIMARY KEY (`product_id`, `order_id`),
    INDEX `fk_product_has_orders_orders1_idx` (`order_id` ASC),
    INDEX `fk_product_has_orders_product_idx` (`product_id` ASC),
    CONSTRAINT `fk_product_has_orders_product`
        FOREIGN KEY (`product_id`)
            REFERENCES `Flevosap`.`product` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_product_has_orders_orders1`
        FOREIGN KEY (`order_id`)
            REFERENCES `Flevosap`.`orders` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Flevosap`.`review`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Flevosap`.`review`;

CREATE TABLE IF NOT EXISTS `Flevosap`.`review`
(
    `id`         INT AUTO_INCREMENT NOT NULL,
    `columns`    VARCHAR(45)        NULL,
    `text`       VARCHAR(45)        NULL,
    `stars`      VARCHAR(45)        NULL,
    `created_at` VARCHAR(45)        NULL,
    `product_id` INT                NOT NULL,
    `user_id`    INT                NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_review_product1_idx` (`product_id` ASC),
    INDEX `fk_review_users1_idx` (`user_id` ASC),
    CONSTRAINT `fk_review_product1`
        FOREIGN KEY (`product_id`)
            REFERENCES `Flevosap`.`product` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_review_users1`
        FOREIGN KEY (`user_id`)
            REFERENCES `Flevosap`.`users` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
