<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?php require "_partials/header.view.php" ?>
        <title>Flevosap-Homepage</title>
    </head>
    <body>

        <?php require "_partials/navbar.view.php" ?>

        <div class="container py-4">
            <div class="card">
                <div class="card-header">
                    Winkelwagen
                </div>
                <?php if (isset($_SESSION['cart']) && count($_SESSION['cart'])) { ?>
                    <?php foreach ($_SESSION['cart'] as $product) { ?>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <img class="w-100" src="<?= $product[0]->image ?>">
                                </div>
                                <div class="col-md-4">
                                    <p class="product-name"><?= $product[0]->name ?></p>
                                    <small class="text-muted">p.st.
                                        &euro;<?= number_format($product[0]->price, 2) ?></small>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Aantal</label>
                                        <p class="mb-0"><?= $product['amount'] ?></p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <a href="/cart/remove?product_id=<?= $product[0]->id ?>"
                                           class="btn btn-outline-danger form-control">X
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-9 text-right">Totaal:
                                    <strong>&euro;<?= number_format($total, 2) ?></strong>
                                </div>
                                <div class="col-md-3">
                                    <a href="/order/create" class="btn btn-success w-100">Bestellen</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <p class="m-3">Er zijn geen producten in je winkelwagen.
                        <a href="/">Bekijk producten</a>
                    </p>
                <?php } ?>
            </div>
        </div>

        <?php require "_partials/footer.view.php" ?>

    </body>
</html>
