<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?php require "_partials/header.view.php" ?>
        <title>Flevosap - Wachtwoord vergeten</title>
    </head>
    <body>

        <?php require '_partials/navbar.view.php' ?>

        <div class="container py-4">
            <!-- foto's van de sappen in een tabel-->
            <h2>Wachtwoord vergeten</h2>

            <div class="card w-100 my-3">
                <form action="">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary" type="submit">Stuur email</button>
                    </div>
                </form>
            </div>

        </div>
        <?php require '_partials/footer.view.php' ?>
    </body>
</html>
