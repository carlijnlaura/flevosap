<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?php require '_partials/header.view.php' ?>
        <title>Flevosap</title>
    </head>
    <body>

        <?php require '_partials/navbar.view.php' ?>

        <div class="container py-4">
            <!-- foto's van de sappen in een tabel-->
            <h2>Flevosappen</h2>
            <p>Welkom bij onze webshop. Wij bieden hier vele verchillende sappen en dranken aan</p>
            <br>
            <div class="row">
                <?php foreach($products as $product) { ?>
                <div class="col-md-4 mb-4">
                    <div class="card">
                        <img class="card-img-top" src="<?php echo $product->image ?>" alt="Appel-citroensap">
                        <div class="card-body">
                            <h4><?php echo $product->name ?></h4><small class="float-right">⭐ ️<?= $product->getStars() ?></small>
                            <p><?php echo $product->ingredients ?></p>
                            &euro;<?php echo number_format($product->price, 2) ?>
                            <small class="float-right">excl. &euro;<?= ($product->price * ((100-9) / 100)) ?></small>
                        </div>
                        <div class="card-footer">
                            <a href="/product?id=<?php echo $product->id ?>" class="btn btn-info">Bekijk product</a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php require '_partials/footer.view.php' ?>
    </body>
</html>
