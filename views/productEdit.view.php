<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Video wijzigen</title>
        <?php require 'views/_partials/header.view.php' ?>

    </head>
    <body>
        <?php require 'views/_partials/navbar.view.php' ?>
        <div class="container">
            <div class="card w-100 mt-4">

                <form action='/save-product' method="POST">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Naam</label>
                            <input type='text' class="form-control"
                                   value='<?= $product->name ?>' name='name'>
                        </div>
                        <div class="form-group">
                            <label>Prijs</label>
                            <textarea name='price' class="form-control" id='price' cols='30'
                                      rows='5'><?= $product->price ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Ingredienten</label>
                            <textarea name='ingredients' class="form-control" id='ingredients' cols='10'
                                      rows='5'><?= $product->ingredients ?></textarea>
                        </div>
                        <input type='hidden' name='id' value='<?= $product->id ?>'>
                    </div>
                    <div class="card-footer">
                        <button type='submit' class='btn btn-primary' value='<?= $product->id ?>'>Opslaan
                        </button>
                    </div>
            </div>
            <?php require '_partials/footer.view.php' ?>
    </body>
</html>
