<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?php require '_partials/header.view.php' ?>

        <title>Flevosap - Product</title>
    </head>
    <body>

        <?php require '_partials/navbar.view.php' ?>

        <div class="container py-4">
            <h2><?= $product->name ?></h2>

            <div class="row my-5">
                <div class="col-md-6">
                    <img class="w-100" src="<?= $product->image ?>" alt="">
                </div>
                <div class="col-md-6 border-left">
                    <h3><?= $product->name ?></h3>
                    <small class="mt-4 d-block">Prijs per stuk</small>
                    <h5 class="font-weight-light">&euro;<?= number_format($product->price, 2) ?></h5>
                    <small class="font-weight-light">excl. &euro;<?= ($product->price * ((100-9) / 100)) ?></small>
                    <p class="mt-2"><?= $product->ingredients ?></p>
                    <form action="/cart/add" method="post">
                        <input type="hidden" value="<?= $product->id ?>" name="id">
                        <div class="form-group my-5">
                            <label>Aantal</label>
                            <input type="number" name="amount" class="form-control" min="1" value="1">
                        </div>
                        <button type="submit" class="btn btn-success"><span class="mr-3">🛒</span> Voeg to aan
                            winkelwagen
                        </button>
                    </form>
                </div>
            </div>

            <div class="row my-5">
                <div class="col-3">
                    <div class="card">
                        <div class="card-body text-center bg-success text-white">
                            <h1><?= number_format($stars, 1) ?></h1>
                            <small><?= count($reviews) ?> recensie(s)</small>
                        </div>
                    </div>
                </div>
                <div class="col-9">
                    <div class="one-review">
                        <p>Recencies</p>
                        <?php foreach($reviews as $review) { ?>
                        <div class="card my-3">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><?= $review['user_name'] ?></p>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <small><?= $review['created_at'] ?></small>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <?= str_repeat('⭐️', $review['stars']) ?>
                                    </div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-12">
                                        <p><?= $review['text'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <?php if (isset($_SESSION['user'])) { ?>
                        <form action="/review/create?product_id=<?= $product->id ?>" method="post">
                            <div class="card mt-3">
                                <div class="card-header">
                                    Schrijf een review
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Sterren</label>
                                        <input type="number" class="form-control" min="1" max="5" name="stars" step="1" value="4">
                                    </div>
                                    <div class="form-group">
                                        <textarea name="text" class="form-control" cols="30" rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-primary">Stuur</button>
                                </div>
                            </div>
                        </form>
                    <?php } ?>
                </div>
            </div>

            <?php require '_partials/footer.view.php' ?>

    </body>
</html>
