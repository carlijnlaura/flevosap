<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?php require "_partials/header.view.php" ?>
        <title>Flevosap-Homepage</title>
    </head>
    <body>

        <?php require "_partials/navbar.view.php" ?>

        <div class="container py-4">
            <h3>Bestelling gelukt!</h3>
            <p>Je bestelling is succesvol ontvangen en wij gaan zo snel mogelijk aan de slag</p>
            <a href="/" class="btn btn-outline-info">Terug naar de homepagina</a>
        </div>

        <?php require "_partials/footer.view.php" ?>

    </body>
</html>
