<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-9">
                <h6>Over ons</h6>
                <p class="text-justify mb-0">Flevosap bv</p>
                <p class="text-justify mb-0">Prof. Zuurlaan 22</p>
                <p class="text-justify mb-0">8256 PE Biddinghuizen, Nederland</p>
                <p class="text-justify mb-0">Tel: +31 (0)321 – 33 25 25</p>
                <p class="text-justify mb-0">info@flevosap.nl</p>
            </div>

            <div class="col-xs-6 col-md-3">
                <h6>Sitemap</h6>
                <ul class="footer-links">
                    <li>
                        <a href="/login">Inloggen</a>
                    </li>
                    <li>
                        <a href="/register">Registreren</a>
                    </li>
                    <li>
                        <a href="mailto:info@flevosap.nl">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
        <hr>
        <p class="copyright-text m-0">Copyright &copy; 2020 All Rights Reserved by
            <a href="/">Flevosap</a>
        </p>
    </div>
</footer>
<script src="/assets/js/jquery-3.5.1.slim.min.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
