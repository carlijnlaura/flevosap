<nav class="navbar navbar-expand-lg navbar-light bg-light p-0">
    <div class="container p-0">
        <a class="navbar-brand py-0" href="/"><img src="/assets/img/flevosap-logo.png" style="height: 60px"
                                                   alt="logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <form action="/search" method="GET" class="form-inline my-2 my-lg-0 mx-auto">
                <div class="input-group">
                    <input type="search" class="form-control" name="q" placeholder="Zoek sappen">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit">🔎</button>
                    </div>
                </div>
            </form>
            <ul class="navbar-nav">
                <?php if (isset($_SESSION['user'])) { ?>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/profile">Profiel</a>
                    </li>
                    <?php if ($_SESSION['user']->isAdmin()) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/orders">Orders</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/admins">Admins</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/users/view">Users</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin">Producten</a>
                        </li>
                    <?php } else { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/user/orders">Orders</a>
                        </li>
                    <?php } ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/logout">Uitloggen</a>
                    </li>
                    <?php } else { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Inloggen</a>
                        </li>
                    <?php } ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/cart">🛒</a>
                    </li>
                </ul>
        </div>
    </div>
</nav>