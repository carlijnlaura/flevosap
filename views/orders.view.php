<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?php require 'views/_partials/header.view.php' ?>
        <title>Flevosap</title>
    </head>
    <body>

        <?php require 'views/_partials/navbar.view.php' ?>

        <div class="container" style="margin-top: 100px;margin-bottom: 100px;">
            <div class="card">
                <div class="card-header">Orders</div>
                <div class="card-body">

                    <?php if(isset($_SESSION['user']) && (count($orders))) { ?>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Order date</th>
                                    <th scope="col">Shipping Date</th>
                                    <th scope="col">Order status</th>
                                    <th width="50" scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($orders as $order) { ?>
                                    <tr>
                                        <td scope="row"><?= $order->id ?></td>
                                        <td scope="row"><?= $order->orderDate ?></td>
                                        <td scope="row"><?= $order->shippingDate ?></td>
                                        <td scope="row"><?= $order->status ?></td>
                                        <td width="50">
                                            <a href="/user/orders/view?order_id=<?= $order->id ?>">View</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } else { ?>
                        <p class="m-0">No orders found</p>
                    <?php } ?>
                </div>
            </div>
        </div>

        <?php require 'views/_partials/footer.view.php' ?>
    </body>
</html>
