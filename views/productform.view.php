<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?php require '_partials/header.view.php' ?>
        <title>Flevosap</title>
    </head>
    <body>

        <?php require '_partials/navbar.view.php' ?>

        <div class="container">
            <div class="card w-100 my-3">
                <form action="/create-product" method="post" enctype="multipart/form-data">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Naam</label>
                                    <input type="text" class="form-control" name="name">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Ingredienten</label>
                                    <input type="text" class="form-control" name="ingredients">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Prijs</label>
                            <input type="number" class="form-control" name="price">
                        </div>
                        <div class="form-group">
                            <label>Categorie</label>
                            <select name="categoryId">
                                <option value="1">Vruchtensap</option>
                                <option value="2">Groentensap</option>
                            </select>
                        </div>
                        <input type="file" name="image"/>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary" type="submit">Voeg toe</button>
                    </div>
                </form>

            </div>
        </div>
        <?php require '_partials/footer.view.php' ?>
    </body>
</html>
