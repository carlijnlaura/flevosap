<?php

require 'tables.view.php';

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?php require "_partials/header.view.php" ?>
        <title>Flevosap - Users</title>
    </head>
    <body>

        <?php require "_partials/navbar.view.php" ?>

        <div class="container">
            <h2>Users list</h2>
            <div class="row">
                <?php foreach ($users

                               as $user) { ?>
                    <div class='col-sm-6'>
                        <div class="card">
                            <div class="card-body">
                                <p>
                                    Gebruiker: <?= $user->email ?>
                                </p>
                                <p>
                                    Naam: <?= $user->first_name ?>
                                </p>
                                <p>
                                    Achternaam: <?= $user->last_name ?>
                                </p>
                                <p>
                                    Telefoonummer: <?= $user->phone_number ?>
                                </p>
                                <p>
                                    Postcode: <?= $user->zip ?>
                                </p>
                                <p>
                                    Straat: <?= $user->street ?>
                                </p>
                            </div>
                            <div class="card-footer d-flex justify-content-between">
                                <form action='/remove-users' method="POST">
                                    <input type='hidden' name='id' value='$id'>
                                    <button type='submit' class='btn btn-warning' value='<?= $user->id ?>'
                                            name='deleteuser'>
                                        Verwijderen
                                    </button>
                                </form>
                                <form action='/edit-users' method="POST">
                                    <input type='hidden' name='id' value='$id'>
                                    <button type='submit' class='btn btn-warning'>Wijzigen</button>
                                </form>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <?php require "_partials/footer.view.php" ?>

    </body>
</html>
