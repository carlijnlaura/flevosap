<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?php include '_partials/header.view.php' ?>

    <title>Flevosap - profile</title>
</head>
<body>
<?php include '_partials/navbar.view.php' ?>
<br><br>
<div class="container">
    <?php
    if (isset($_GET['msg']) && isset($_GET['type'])) {

        ?>
        <div class="alert alert-<?= $_GET['type'] ?>">
            <?= $_GET['msg']; ?>
        </div>
        <?php
    }
    ?>
    <div class="row my-2">
        <div class="col-lg-8 order-lg-2">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Profiel</a>
                </li>
                <li class="nav-item">
                    <a href="" data-target="#edit" data-toggle="tab" class="nav-link">Wijzig</a>
                </li>
            </ul>
            <div class="tab-content py-4">
                <div class="tab-pane active" id="profile">
                    <h5 class="mb-3">Welkom bij je profiel!</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <?php
                            if (isset($_SESSION['user'])) { ?>
                                <h6>Uw gegevens</h6>
                                <p>
                                    Hoe verander ik mijn account gegevens?<br> Naast de profiel sectie kunt u de wijzig
                                    sectie vinden. Hier kunt u uw gegevens veranderen of uw profiel verwijderen.
                                </p>
                                <p> Email: <?= $_SESSION['user']->email; ?></p>

                            <?php } else {
                                echo "Nothing found";
                            } ?>
                        </div>
                    </div>
                    <!--/row-->
                </div>
                <div class="tab-pane" id="edit">
                    <form method="post" action="/profile/update" role="form">
                        <input type="hidden" value="<?= $_SESSION['user']->id; ?>" name="id">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Voornaam</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" value="<?= $_SESSION['user']->first_name; ?>"
                                       name="first_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Volledige achternaam </label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" value="<?= $_SESSION['user']->last_name; ?>"
                                       name="last_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Email</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="email" value="<?= $_SESSION['user']->email; ?>"
                                       name="email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Telefoon nummer</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="tel" value="<?= $_SESSION['user']->phone_number; ?>"
                                       name="phone_number">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Address</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" value="<?= $_SESSION['user']->street; ?>"
                                       placeholder="Straat" name="street">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label"></label>
                            <div class="col-lg-6">
                                <input class="form-control" type="text" value="<?= $_SESSION['user']->city; ?>"
                                       placeholder="Stad" name="city">
                            </div>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" value="<?= $_SESSION['user']->zip; ?>"
                                       placeholder="Postcode" name="zip">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Oud wachtwoord</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="password" placeholder="Oud wachtwoord" name="old_password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Nieuw wachtwoord</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="password" placeholder="Wachtwoord" name="new_password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Bevestig wachtwoord</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="password" placeholder="Bevestig wachtwoord" name="new_password2">
                            </div>
                        </div>
                        <br>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label"></label>
                            <div class="col-lg-9">
                                <input type="reset" class="btn btn-secondary" value="Annuleren">
                                <button class="btn btn-primary" type="submit">Opslaan</button>
                            </div>
                        </div>
                    </form>

                    <br><br>
                    <form action="/profile/delete" method="post">
                        <input type="hidden" value="<?= $_SESSION['user']->id; ?>" name="id">
                        <div class="form-group row">
                            <label class="col-lg-9 col-form-label form-control-label">Door op de 'Verwijderen" knop te
                                klikken, verwijdert u uw account & gegevens. </label>
                            <p class="col-lg-9 text-danger">Pas op: dit kan niet teruggedraaid worden!</p>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-9">
                                <input type="submit" class="btn btn-danger" value="Verwijderen">
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="col-lg-4 order-lg-1 text-center">
            <img src="/assets/img/apple-profile-pic.jpeg" class="mx-auto img-fluid img-circle d-block img-thumbnail"
                 alt="avatar">
        </div>
    </div>
</div>


<?php include '_partials/footer.view.php' ?>
</body>
</html>
