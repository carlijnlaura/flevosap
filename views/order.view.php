<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?php require 'views/_partials/header.view.php' ?>
        <title>Flevosap</title>
    </head>
    <body>

        <?php require 'views/_partials/navbar.view.php' ?>

        <div class="container" style="margin-top: 100px;margin-bottom: 100px;">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div>Order #<?= $order->id ?></div>
                    <!--                    <a href="" class="btn btn-primary" target="_blank">Label</a>-->
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p>Order date:</p>
                            <?= $order->orderDate ?>
                        </div>
                        <div class="col-md-6">
                            <p>Shipping date:</p>
                            <?= $order->shippingDate ?>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <p>Status:</p>
                            <?= $order->status ?>
                        </div>
                        <div class="col-md-6">
                            <p>Klant</p>
                            <?= $order->Customer()->first_name ?> <?= $order->Customer()->last_name ?>
                        </div>
                    </div>
                    <hr>
                    <?php foreach ($order->OrderedProducts() as $orderedProduct) { ?>
                        <div class="row">
                            <div class="col-3">
                                <p>Product</p>
                                <?= $orderedProduct->Product()->name ?>
                            </div>
                            <div class="col-3">
                                <p>Prijs per stuk</p>
                                &euro;<?= number_format($orderedProduct->Product()->price, 2) ?>
                            </div>
                            <div class="col-3">
                                <p>Aantal</p>
                                <?= $orderedProduct->quantity ?>x
                            </div>
                            <div class="col-3">
                                <p>Totaal</p>
                                &euro;<?= number_format($orderedProduct->quantity * $orderedProduct->Product()->price, 2) ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <?php require 'views/_partials/footer.view.php' ?>
    </body>
</html>
