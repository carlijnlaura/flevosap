<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?php require '_partials/header.view.php' ?>

    <title>Flevosap - Login</title>
</head>
<body>

<?php require '_partials/navbar.view.php' ?>

<div class="container py-4">

    <!-- foto's van de sappen in een tabel-->
    <?php
    if(isset($_GET['msg'])){

        ?>
        <div class="alert alert-danger">
            <?= $_GET['msg']; ?>
        </div>
        <?php
    }
    ?>
    <h2>Inloggen</h2>

    <div class="card w-100 my-3">
        <form action="/login-post" method="post">
            <div class="card-body">
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email">
                </div>
                <div class="form-group">
                    <label>Wachtwoord</label>
                    <input type="password" class="form-control" name="password">
                    <a href="">Wachtwoord vergeten?</a>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary" type="submit">Log in</button>
            </div>
        </form>
    </div>

    <p>Nog geen account?
        <a href="/register">klik hier om een account aan te maken</a>
    </p>

</div>
<?php require '_partials/footer.view.php' ?>
</body>
</html>
