<?php

require 'tables.view.php';


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?php require '_partials/header.view.php' ?>
        <title>Flevosap</title>
    </head>
    <body>

        <?php require '_partials/navbar.view.php' ?>

        <div class="container">
            <div class="d-flex justify-content-between">
                <h4>Producten</h4>
                <a class="btn btn-sm btn-primary" href="/product/create">Maak product</a>
            </div>

            <div class="row text-center py-5">


                <?php
                foreach ($products

                as $product) { ?>

                <?php
                component($product->name, $product->price, $product->ingredients, $product->image, $product->id);

                ?>
            </div>

            <?php
            }
            ?>
        </div>
        <?php require '_partials/footer.view.php' ?>


    </body>
</html>
