<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/main.css">
    <title>Flevosap - Registreren</title>
</head>
<body>

<?php include '_partials/navbar.view.php' ?>
<div class="container py-4">

    <?php
    if(isset($_GET['msg'])){
        ?>
        <div class="alert alert-danger">
            <?= $_GET['msg']; ?>
        </div>
        <?php
    }
    ?>


    <h2>Registreren</h2>

    <div class="card w-100 my-3">
        <form action="/register-post" method="post">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label>Voornaam</label>
                            <input type="text" class="form-control" name="first_name">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label>Achternaam</label>
                            <input type="text" class="form-control" name="last_name">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Telefoon nummer</label>
                    <input type="number" class="form-control" name="phone_number">
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Postcode</label>
                            <input type="text" class="form-control" name="zip">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label>Plaats</label>
                            <input type="text" class="form-control" name="city">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label>Straat inc Nr</label>
                            <input type="text" class="form-control" name="street">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email">
                </div>
                <div class="form-group">
                    <label>Wachtwoord</label>
                    <input type="password" class="form-control" name="password">
                </div>

            </div>
            <div class="card-footer">
                <button class="btn btn-primary" type="submit">Registreer</button>
            </div>
        </form>
    </div>
    <p>Al een account?
        <a href="/login">klik hier om in te loggen</a>
    </p>
</div>
<?php require '_partials/footer.view.php' ?>
</body>
</html>
