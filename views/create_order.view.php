<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?php require "_partials/header.view.php" ?>
        <title>Flevosap-Homepage</title>
    </head>
    <body>

        <?php require "_partials/navbar.view.php" ?>

        <div class="container py-4">
            <form action="/order/createpost" method="POST">
                <div class="card">
                    <div class="card-header">
                        Gegevens
                    </div>
                    <div class="card-body">
                        <?php if (isset($_SESSION['user'])) { ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Voornaam</label>
                                        <input type="text" required class="form-control" name="first_name" value="<?= $_SESSION['user']->first_name ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Achternaam</label>
                                        <input type="text" required class="form-control" name="last_name" value="<?= $_SESSION['user']->last_name ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" required class="form-control" name="email" value="<?= $_SESSION['user']->email ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Telefoonnummer</label>
                                        <input type="number" required class="form-control" name="phone_number" value="<?= $_SESSION['user']->phone_number ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Straat incl nummer</label>
                                        <input type="text" required class="form-control" name="street" value="<?= $_SESSION['user']->street ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Postcode</label>
                                        <input type="text" required class="form-control" name="zip" value="<?= $_SESSION['user']->zip ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Stad</label>
                                        <input type="text" required class="form-control" name="city" value="<?= $_SESSION['user']->city ?>">
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Voornaam</label>
                                        <input type="text" required class="form-control" name="first_name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Achternaam</label>
                                        <input type="text" required class="form-control" name="last_name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" required class="form-control" name="email">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Telefoonnummer</label>
                                        <input type="number" required class="form-control" name="phone_number">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Straat incl nummer</label>
                                        <input type="text" required class="form-control" name="street">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Postcode</label>
                                        <input type="text" required class="form-control" name="zip">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Stad</label>
                                        <input type="text" required class="form-control" name="city">
                                    </div>
                                </div>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1"
                                       onchange="$('#password').toggleClass('d-none')" name="createaccount">
                                <label class="form-check-label" for="exampleCheck1">Maak account voor mij aan</label>
                            </div>
                            <div class="form-group d-none" id="password">
                                <hr>
                                <label>Wachtwoord</label>
                                <input type="password" name="password" class="form-control">
                            </div>
                        <?php } ?>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-success float-right">Bestel</button>
                    </div>
                </div>
            </form>
        </div>

        <?php require "_partials/footer.view.php" ?>

    </body>
</html>
