<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Flevosap</title>
        <?php require 'views/_partials/header.view.php' ?>
    </head>
    <body>
        <?php require 'views/_partials/navbar.view.php' ?>
        <div class="container" style="margin-top: 100px;margin-bottom: 100px;">
            <?php if(isset($_GET['msg'])) { ?>
                <div class="alert alert-warning" role="alert">
                    <?= $_GET['msg'] ?>
                </div>
            <?php } ?>
            <div class="card">
                <div class="card-header">
                    Admins
                </div>
                <form action="/admin/admins/update?admin_id=<?= $admin->id ?>" method="POST">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First name</label>
                                    <input type="text" class="form-control" value="<?= $admin->first_name ?>" name="first_name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last name</label>
                                    <input type="text" class="form-control" value="<?= $admin->last_name ?>" name="last_name">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" value="<?= $admin->email ?>" name="email">
                        </div>
                        <?php if($admin->id !== $_SESSION['user']->id) { ?>
                            <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1" name="is_admin" checked="checked">
                            <label class="form-check-label" for="exampleCheck1">Is admin</label>
                        </div>
                        <?php } else { ?>
                            <small class="text-muted">You cannot remove yourself</small>
                            <input type="hidden" name="is_admin" value="true">
                        <?php } ?>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>

        <?php require 'views/_partials/footer.view.php' ?>
    </body>
</html>