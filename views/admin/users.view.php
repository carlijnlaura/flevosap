<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin - user information</title>
    <?php require 'views/_partials/header.view.php' ?>
</head>
<body>
<?php require 'views/_partials/navbar.view.php' ?>

<div class="container" style="margin-top: 100px;margin-bottom: 100px;">
    <h2>User list</h2>
    <div class="row">
        <?php if(count($users)):
            foreach($users as $user): ?>

                <div class="col-sm-3">
                    <div class="card" style="height: 1155px; width: 275px; margin-bottom: 10px;">
                        <div class="card-body">
                            <p class="card-text"><b>Volledige naam:</b><br> <?= $user->first_name . " " . $user->last_name ?></p>
                            <p class="card-text"><b>Email:</b><br> <?= $user->email ?></p>
                            <p class="card-text"><b>Telefoonnummer:</b><br> <?= $user->phone_number ?></p>
                            <p class="card-text"><b>Postcode:</b><br> <?= $user->zip ?></p>
                            <p class="card-text"><b>Straat:</b><br> <?= $user->street ?></p>
                            <p class="card-text"><b>Stad:</b><br> <?= $user->city ?></p>
                            <p class="card-text"><b>Wijzig gebruiker:</b></p>
                            <form method="post" action="/admin/user/update" role="form">
                                <input type="hidden" value="<?= $user->id ?>" name="id">
                                <input class="form-control" value="<?= $user->first_name ?>" type="text" name="first_name"><br>
                                <input class="form-control" value="<?= $user->last_name?>" type="text" name="last_name"><br>
                                <input class="form-control" value="<?= $user->email ?>" type="email" name="email"><br>
                                <input class="form-control" value="<?= $user->phone_number ?>" type="number" name="phone_number"><br>
                                <input class="form-control" value="<?= $user->zip ?>" type="text" name="zip"><br>
                                <input class="form-control" value="<?= $user->street ?>" type="text" name="street"><br>
                                <input class="form-control" value="<?= $user->city ?>" type="text" name="city"><br>
                                <p class="card-title"><b>Vul het wachtwoord veld in om het wachtwoord te wijzigen:</b></p>
                                <input class="form-control" type="password" placeholder="New password" name="password"><br>

                                <br>
                                <button class="btn btn-primary" type="submit">Opslaan</button>
                            </form>
                            <br>
                            <form action="/admin/user/delete" method="post">
                                <input type="hidden" value="<?= $user->id ?>" name="id">
                                <input type="submit" class="btn btn-danger" value="Verwijderen">
                            </form>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
<?php //require '_partials/footer.view.php' ?>

</body>
</html>