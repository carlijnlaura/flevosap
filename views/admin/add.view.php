<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Flevosap</title>
        <?php require 'views/_partials/header.view.php' ?>
    </head>
    <body>
        <?php require 'views/_partials/navbar.view.php' ?>
        <div class="container" style="margin-top: 100px;margin-bottom: 100px;">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div>Admins</div>
                </div>
                <form action="/admin/admins/add" method="post">
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th width="50">Admin?</th>
                            </tr>
                            <?php foreach ($users as $user) { ?>
                                <tr>
                                    <td><?= $user->id ?></td>
                                    <td><?= $user->name ?></td>
                                    <td><?= $user->email ?></td>
                                    <td>
                                        <input type="checkbox" name="users[]" value="<?= $user->id ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary">Add selected as admin</button>
                    </div>
                </form>
            </div>
        </div>

        <?php require 'views/_partials/footer.view.php' ?>
    </body>
</html>