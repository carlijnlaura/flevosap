<?php

class Order extends BaseModel
{

    public $id;
    public $orderDate;
    public $shippingDate;
    public $status;
    public $userId;
    public $email;
    public $first_name;
    public $last_name;
    public $phone_number;
    public $zip;
    public $street;
    public $city;
    public $password;

    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->orderDate = $data['orderdate'];
        $this->shippingDate = $data['shippingDate'];
        $this->status = $data['status'];
        $this->userId = $data['user_id'];
        $this->email = $data['email'];
        $this->first_name = $data['first_name'];
        $this->last_name = $data['last_name'];
        $this->phone_number = $data['phone_number'];
        $this->zip = $data['zip'];
        $this->street = $data['street'];
        $this->city = $data['city'];
        $this->password = $data['password'];
        $this->name = $data['name'];
    }

    public static function all()
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("SELECT * FROM orders");
        $arr = [];
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            foreach ($stmt->fetchAll() as $order) {
                $arr[] = new Order($order);
            }
        }
        return $arr;
    }

    public static function Userorders()
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("SELECT * FROM orders WHERE user_id = :user_id");
        $stmt->bindParam(':user_id', $_SESSION['user']->id, PDO::PARAM_INT);
        $arr = [];
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            foreach ($stmt->fetchAll() as $order) {
                $arr[] = new Order($order);
            }
        }
        return $arr;
    }

    public static function find($id)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("SELECT * FROM orders WHERE id = :id LIMIT 1");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return new Order($stmt->fetch());
        }
        return null;
    }

    public function Customer()
    {
        return User::getUser($this->userId);
    }

    public function OrderedProducts()
    {
        return OrderedProduct::getByOrderId($this->id);
    }

    public static function create($data)
    {
        $pdo = self::connect();
        $sql = "insert into orders (orderdate, shippingDate, status, user_id, first_name, last_name, phone_number, zip, street, city, email) values (:orderdate, :shippingdate, :status, :user_id, :first_name, :last_name, :phone_number, :zip, :street, :city, :email)";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':user_id', $data['user_id'], PDO::PARAM_INT);
        $stmt->bindParam(':orderdate', $data['orderdate'], PDO::PARAM_STR);
        $stmt->bindParam(':shippingdate', $data['shippingdate'], PDO::PARAM_STR);
        $stmt->bindParam(':status', $data['status'], PDO::PARAM_INT);
        $stmt->bindParam(':email', $data['email'], PDO::PARAM_STR);
        $stmt->bindParam(':first_name', $data['first_name'], PDO::PARAM_STR);
        $stmt->bindParam(':last_name', $data['last_name'], PDO::PARAM_STR);
        $stmt->bindParam(':phone_number', $data['phone_number'], PDO::PARAM_STR);
        $stmt->bindParam(':zip', $data['zip'], PDO::PARAM_STR);
        $stmt->bindParam(':street', $data['street'], PDO::PARAM_STR);
        $stmt->bindParam(':city', $data['city'], PDO::PARAM_STR);

        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return Order::find($pdo->lastInsertId());
        }
        return null;
    }


}