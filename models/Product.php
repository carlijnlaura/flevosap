<?php

class Product extends BaseModel //Maak klasse Product aan.
{

    public $id;
    public $name;
    public $price;
    public $image;
    public $ingredients;
    public $category_id;

    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->name = $data['name'];
        $this->price = $data['price'];
        $this->image = $data['image'];
        $this->ingredients = $data['ingredients'];
        $this->category_id = $data['category_id'];
    }

    public function findProduct($id)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("SELECT * FROM products WHERE id = :id LIMIT 1");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return new Product($stmt->fetch());
        }
        return null; //return nohting
    }

    public static function getProduct($id)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("select * from product where id = :id"); // hier bereiden we het sql statement voor.
        $stmt->bindParam(':id', $id, PDO::PARAM_INT); // bind de id
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return new Product($stmt->fetch());
        }
        return null; //return nohting
    }

    public static function getallproducts() //Roep functie getallproducts aan.
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("select * from product"); // hier bereiden we het sql statement voor.
        $arr = [];
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            foreach ($stmt->fetchAll() as $product) {
                $arr[] = new Product($product);
            }
        }
        return $arr;
    }

    public function delete()
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("DELETE FROM product WHERE id = :id");
        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
        return $stmt->execute();
    }

    public function updateProduct($data)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("UPDATE product SET name = :name, price = :price, ingredients = :ingredients WHERE id = :id");
        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT); // bind de id
        $stmt->bindParam(':name', $data['name'], PDO::PARAM_STR_CHAR); // bind de id
        $stmt->bindParam(':price', $data['price'], PDO::PARAM_STR_CHAR); // bind de id
        $stmt->bindParam(':ingredients', $data['ingredients'], PDO::PARAM_STR_CHAR); // bind de id
        return $stmt->execute();
    }

    public static function searchProducts($query)
    {
        $query = '%' . $query . '%';
        $pdo = self::connect();
        $stmt = $pdo->prepare("select * from product where name LIKE :name OR ingredients LIKE :ingredients");
        $stmt->bindParam(':name', $query, PDO::PARAM_STR_CHAR);
        $stmt->bindParam(':ingredients', $query, PDO::PARAM_STR_CHAR);
        $arr = [];
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            foreach ($stmt->fetchAll() as $product) {
                $arr[] = new Product($product);
            }
        }
        return $arr;
    }


    public static function create($data)
    {
        $pdo = self::connect();
        $random = random_int(1000, 100000);
        $storePath = "/assets/upload/" . $random . ".png";
        $fullPath = dirname(__DIR__, 1) . "/assets/upload/" . $random . ".png";

        $allowed = array('jpeg', 'png', 'jpg');
        $filename = $_FILES['image']['name'];

        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if (!in_array($ext, $allowed)) {
            echo "Sorry, only JPG, JPEG, PNG & GIF  files are allowed.";
        } else {
            move_uploaded_file($_FILES['image']['tmp_name'], $fullPath);
            $stmt = $pdo->prepare("INSERT INTO product (name, price, ingredients, category_id, image) VALUES (:name, :price, :ingredients, :categoryId, :image)");
            $stmt->bindValue(':name', $data['name']);
            $stmt->bindValue(':price', $data['price']);
            $stmt->bindValue(':ingredients', $data['ingredients']);
            $stmt->bindValue(':categoryId', $data['categoryId']);
            $stmt->bindParam(':image', $storePath);
            return $stmt->execute();
        }
        return false;
    }

    public function getStars()
    {
        $reviews = Review::getReviewsForProduct($this->id);

        $review_total = 0;
        $stars = 0;

        foreach ($reviews as $review) {
            $review_total += $review['stars'];
        }

        if (count($reviews) > 0) {
            $stars = $review_total / count($reviews);
        }
        return number_format($stars, 1);
    }

}



