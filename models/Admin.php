<?php

class Admin extends BaseModel
{

    public $id;
    public $name;
    public $email;
    public $first_name;
    public $last_name;

    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->name = $data['name'];
        $this->email = $data['email'];
        $this->first_name = $data['first_name'];
        $this->last_name = $data['last_name'];
    }

    public static function all()
    {
        $users = User::AllUser();
        $admins = [];
        foreach ($users as $user) {
            if ($user->isAdmin()) {
                $admins[] = new Admin(get_object_vars($user));
            }
        }
        return $admins;
    }

    public static function find($id)
    {
        return new Admin(get_object_vars(User::getUser($id)));
    }

    public function delete()
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("DELETE FROM admin WHERE user_id = :userid");
        $stmt->bindParam(':userid', $this->id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function update($data)
    {
        $pdo = parent::connect();
        if ($this->email === $data['email']) {
            $stmt = $pdo->prepare("UPDATE users SET first_name = :first_name, last_name = :last_name WHERE id= :id"); //update user statement
        } else {
            Admin::checkAdmin($data['email'], $this->id);
            $stmt = $pdo->prepare("UPDATE users SET email = :email, first_name = :first_name, last_name = :last_name WHERE id= :id"); //update user statement
            $stmt->bindValue(':email', $data['email']);
        }
        $stmt->bindValue(':first_name', $data['first_name']);
        $stmt->bindValue(':last_name', $data['last_name']);
        $stmt->bindValue(':id', $this->id);
        return $stmt->execute();
    }

    public static function checkAdmin($email, $id)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("SELECT COUNT(email) AS num FROM users WHERE email = :email"); //check if user exist with the email
        $stmt->bindValue(':email', $email);
        $stmt->execute();
        $row = $stmt->fetch();
        if ($row['num'] > 0) {
            header('Location: /admin/admins/edit?admin_id=' . $id . '&msg=Gebruiker bestaat al');
            exit;
        }
    }

}