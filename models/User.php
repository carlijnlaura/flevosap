<?php

class User extends BaseModel
{

    public $id;
    public $email;
    public $first_name;
    public $last_name;
    public $phone_number;
    public $zip;
    public $street;
    public $city;
    public $password;
    public $name;

    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->email = $data['email'];
        $this->first_name = $data['first_name'];
        $this->last_name = $data['last_name'];
        $this->phone_number = $data['phone_number'];
        $this->zip = $data['zip'];
        $this->street = $data['street'];
        $this->city = $data['city'];
        $this->password = $data['password'];
        $this->name = $data['first_name'] . ' ' . $data['last_name'];
    }

    public static function getUser($id)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("select * from users where id = :id"); // hier bereiden we het sql statement voor.
        $stmt->bindParam(':id', $id, PDO::PARAM_INT); // bind de id
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return new User($stmt->fetch());
        }
        return null; //return nohting
    }


    public static function AllUser()
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("select * from users"); // hier bereiden we het sql statement voor.
        $arr = [];
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            foreach ($stmt->fetchAll() as $product) {
                $arr[] = new User($product);
            }
        }
        return $arr;
    }

    public function EditUser($data)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("UPDATE users SET email = :email, password = :password, name = :name, lastname = :lastname, 
                phoneNumber = :phoneNumber, postalCode = :postalCode, street = :street WHERE id = :id");
        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT); // bind de id
        $stmt->bindParam(':email', $data['email'], PDO::PARAM_STR_CHAR); // bind de id
        $stmt->bindParam(':password', $data['password'], PDO::PARAM_STR_CHAR); // bind de id
        $stmt->bindParam(':name', $data['name'], PDO::PARAM_STR_CHAR); // bind de id
        $stmt->bindParam(':lastname', $data['lastname'], PDO::PARAM_STR_CHAR); // bind de id
        $stmt->bindParam(':phoneNumber', $data['phoneNumber'], PDO::PARAM_STR_CHAR); // bind de id
        $stmt->bindParam(':postalCode', $data['postalCode'], PDO::PARAM_STR_CHAR); // bind de id
        $stmt->bindParam(':street', $data['street'], PDO::PARAM_STR_CHAR); // bind de id
        return $stmt->execute();
    }

    public static function removeUser($id)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("DELETE FROM users WHERE id = :id");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        return $stmt->execute();
    }

    public static function tryLogin($email, $password)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("SELECT * FROM users WHERE email= :email LIMIT 1"); //search if user exist with send email
        $stmt->bindParam(':email', $email);
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            $user = $stmt->fetch(); //fetch the found user
            if (password_verify($password, $user['password'])) { //verify user passwordt with send password
                return new User($user); //if true return the user
            } else {
                header('Location: /login?msg=Verkeerd wachtwoord');
                exit;
            }
        }
        header('Location: /login?msg=Account bestaat niet');
        exit;
    }

    public static function createUser($data)
    {
        $pdo = self::connect();
        User::checkUser($data['email'], 'register');
        $passwordHash = password_hash($data['password'], PASSWORD_BCRYPT);
        $stmt = $pdo->prepare("INSERT INTO users (email, password, first_name, last_name, phone_number, zip, street, city) VALUES (:email, :password, :first_name, :last_name, :phone_number, :zip, :street, :city)");

        $stmt->bindValue(':email', $data['email']);
        $stmt->bindValue(':password', $passwordHash);
        $stmt->bindValue(':first_name', $data['first_name']);
        $stmt->bindValue(':last_name', $data['last_name']);
        $stmt->bindValue(':phone_number', $data['phone_number']);
        $stmt->bindValue(':zip', $data['zip']);
        $stmt->bindValue(':street', $data['street']);
        $stmt->bindValue(':city', $data['city']);

        //If the signup process is successful.
        if ($stmt->execute()) {
            //creating new user
            return User::getUser($pdo->lastInsertId()); //finding user with the last inserted id that pdo returns
        } else { //If the signup process is wrong
            die(var_dump($stmt->debugDumpParams()));
            header('Location: /register?msg=Er is iets misgegaan!');
            exit;
        }
    }

    public static function checkUser($email, $location)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("SELECT COUNT(email) AS num FROM users WHERE email = :email"); //check if user exist with the email
        //Bind the provided email to our prepared statement.
        $stmt->bindValue(':email', $email);
        //Execute.
        $stmt->execute();
        //Fetch the row.
        $row = $stmt->fetch();
        //If the provided username already exists - display error.
        //Killing the script completely.
        if ($row['num'] > 0) {
            header('Location: /' . $location . '?msg=Gebruiker bestaat al');
            exit;
        }
        //if nothing found, just continue the script
    }

    public static function checkUserNoLocation($email)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("SELECT COUNT(email) AS num FROM users WHERE email = :email");
        $stmt->bindValue(':email', $email);
        $stmt->execute();
        $row = $stmt->fetch();
        if ($row['num'] > 0) {
            //user exists
            return true;
        }
        return false;
    }


    public static function verifyUser($id, $password)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("SELECT * FROM users WHERE id = :id LIMIT 1"); //search if user exist with send email
        $stmt->bindParam(':id', $id);
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            $user = $stmt->fetch(); //fetch the found user
            if (password_verify($password, $user['password'])) { //verify user passwordt with send password
                return true; //if true return the user
            } else {
                header('Location: /profile?msg=Wrong password&type=danger');
                exit;
            }
        }
    }

    public static function updateUser($data)
    {
        $user = User::getUser($data['id']);
        if ($user) {
            $pdo = self::connect();

            if($data['email'] == $user->email){
                $stmt = $pdo->prepare("UPDATE users SET first_name = :first_name, last_name = :last_name, phone_number = :phone_number, zip = :zip, street = :street, city = :city, password = :password WHERE id= :id"); //update user statement
            }else{
                $stmt = $pdo->prepare("UPDATE users SET email = :email, first_name = :first_name, last_name = :last_name, phone_number = :phone_number, zip = :zip, street = :street, city = :city, password = :password WHERE id= :id"); //update user statement
            }

            if($data['email'] != $user->email){
                $stmt->bindValue(':email', $data['email']);

            }
            $stmt->bindValue(':first_name', $data['first_name']);
            $stmt->bindValue(':last_name', $data['last_name']);
            $stmt->bindValue(':phone_number', $data['phone_number']);
            $stmt->bindValue(':zip', $data['zip']);
            $stmt->bindValue(':city', $data['city']);
            $stmt->bindValue(':street', $data['street']);
            $stmt->bindValue(':id', $user->id);
            if ($data['old_password'] !== '' && $data['new_password'] !== '' && $data['new_password2'] !== '') {
                if (User::verifyUser($data['id'], $data['old_password']) && $data['new_password'] == $data['new_password2']) {
                    $stmt->bindValue(':password', password_hash($data['new_password'], PASSWORD_BCRYPT));
                }
            }else{
                $stmt->bindValue(':password', $user->password);

            }
//            echo "\nPDO::errorInfo():\n";
//            print_r($stmt->errorInfo());
//            exit;
            if ($stmt->execute()) {
                return User::getUser($user->id); //finding user with the last inserted id that pdo returns
            } else { //If the signup process is wrong
                header('Location: /profile?msg=Something went wrong');
                exit;
            }
        } else {
            header('Location: /profile?msg=User not found');
            exit;
        }
    }

    public static function deleteUser($id)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("DELETE FROM users WHERE id= :id"); //update user statement
        $stmt->bindValue(':id', $id);
        if ($stmt->execute()) {
            return true;
        } else { //If the signup process is wrong
            header('Location: /profile?msg=Something went wrong');
            exit;
        }
    }

    public static function AllUsersAdmin()
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("select * from users"); // hier bereiden we het sql statement voor.
        $arr = [];
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            foreach ($stmt->fetchAll() as $user) {
                $arr[] = new User($user);
            }
        }
        return $arr;
    }

    public static function deleteUsersAdmin($id)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("DELETE FROM users WHERE id= :id"); //update user statement
        $stmt->bindValue(':id', $id);
        if ($stmt->execute()) {
            return true;
        } else { //If the signup process is wrong
            header('Location: /admin?msg=Something went wrong');
            exit;
        }
    }

    public static function updateUsersAdmin($data)
    {
        $user = User::getUser($data['id']);
        if ($user) {
            $pdo = self::connect();
            $stmt = $pdo->prepare("UPDATE users SET email = :email, first_name = :first_name, last_name = :last_name, phone_number = :phone_number, 
                                            zip = :zip, street = :street, city = :city, password = :password WHERE id= :id"); //update user statement
            $stmt->bindValue(':email', $data['email']);
            $stmt->bindValue(':first_name', $data['first_name']);
            $stmt->bindValue(':last_name', $data['last_name']);
            $stmt->bindValue(':phone_number', $data['phone_number']);
            $stmt->bindValue(':zip', $data['zip']);
            $stmt->bindValue(':city', $data['city']);
            $stmt->bindValue(':street', $data['street']);
            $stmt->bindValue(':id', $user->id);
            if($data['password'] == ''){
                $stmt->bindValue(':password', $user->password);
            }else{
                $stmt->bindValue(':password', password_hash($data['password'],PASSWORD_BCRYPT));
            }
            if ($stmt->execute()) {
                return true;
            } else { //If the signup process is wrong
                header('Location: /admin?msg=Something went wrong');
            }
        }
    }

    public function IsAdmin()
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("SELECT * FROM admin where user_id= :userid LIMIT 1");
        $stmt->bindValue(':userid', $this->id);
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return true;
        }
        return false;
    }

    public function makeAdmin()
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("INSERT INTO admin (user_id) VALUES (:userid)");
        $stmt->bindValue(':userid', $this->id);
        $stmt->execute();
    }



}