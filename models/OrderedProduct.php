<?php

class OrderedProduct extends BaseModel
{

    public $product_id;
    public $order_id;
    public $quantity;

    public function __construct($data)
    {
        $this->product_id = $data['product_id'];
        $this->order_id = $data['order_id'];
        //typo
        $this->quantity = $data['quanity'];
    }

    public static function getByOrderId($order_id)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("select * from OrderedProduct where order_id = :order_id");
        $stmt->bindParam(':order_id', $order_id, PDO::PARAM_INT);
        $arr = [];
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            foreach ($stmt->fetchAll() as $order) {
                $arr[] = new OrderedProduct($order);
            }
        }
        return $arr;
    }

    public static function create($data)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("insert into OrderedProduct (product_id, order_id, quanity) values (:product_id, :order_id, :quantity)");
        $stmt->bindParam(':product_id', $data['product_id'], PDO::PARAM_INT);
        $stmt->bindParam(':order_id', $data['order_id'], PDO::PARAM_INT);
        $stmt->bindParam(':quantity', $data['quantity'], PDO::PARAM_INT);
        if($stmt->execute() && $stmt->rowCount() > 0) {
            return true;
        }
        return null;
    }

    public function Product()
    {
        return Product::getProduct($this->product_id);
    }

}