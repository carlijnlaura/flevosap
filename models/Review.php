<?php

class Review extends BaseModel
{

    public $id;
    public $text;
    public $stars;
    public $created_at;
    public $product_id;
    public $user_id;

    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->text = $data['text'];
        $this->stars = $data['stars'];
        $this->created_at = $data['created_at'];
        $this->product_id = $data['product_id'];
        $this->user_id = $data['user_id'];
    }

    public static function getById($id)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("SELECT * FROM review WHERE id = :id LIMIT 1");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return new Review($stmt->fetch());
        }
        return null;
    }

    public static function getReviewsForProduct($id)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("select * from review where product_id = :id");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $data = [];
        foreach ($stmt->fetchAll() as $review) {
            $u = User::getUser($review['user_id']);
            $review['user_name'] = $u->first_name . ' ' . $u->last_name;
            $data[] = $review;
        }
        return $data;
    }

    public static function createReview($text, $stars, $product_id, $user_id)
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("insert into review (stars, text, user_id, product_id) values (:stars, :text, :user_id, :product_id)");
        $stmt->bindParam(':stars', $stars, PDO::PARAM_INT);
        $stmt->bindParam(':text', $text, PDO::PARAM_STR);
        $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $stmt->bindParam(':product_id', $product_id, PDO::PARAM_INT);
        $stmt->execute();
        return Review::getById($pdo->lastInsertId());
    }

}